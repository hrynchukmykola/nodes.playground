class Node {
    var value: String?
    var next: Node?
    var prev: Node?
    var isTail : Bool = false
    var isHead : Bool = false
    
    
}

class LinkedList {
    var head = Node()
    var count = 0
    
    func insert(value: String, index: Int) {
        if self.head.value == nil {
            self.head.value = value
            self.head.isHead = true
            self.head.isTail = true
        } else {
            var last = self.head
            if last.isTail != true {
                while last.next?.isTail != true {
                    last = last.next!
                }
                last = last.next! }
            let new = Node()
            new.value = value
            new.next = head
            new.prev = last
            last.isTail = false
            new.isTail = true
            if index > 0 {
                last.next = new
                
            } else {
                self.head.prev = new
                
            }
        }
        count += 1
    }
    
    public func nodeAt(index: Int) -> Node? {
        
        if index >= 0 {
            var node = self.head
            var i = 0
            
            while node.next != nil {
                if i == index {
                    return node }
                i += 1
                node = node.next!
            }
        }
        
        return nil
    }
    func removeAt (index: Int) {
        let objToRemove = nodeAt(index: index)
        if objToRemove == nil {print("invalid index")}
        let previousNode = objToRemove?.prev
        let nextNode = objToRemove?.next
        previousNode?.next = nextNode
        nextNode?.prev = previousNode
    }
    
    func reverse () {
        var curr = head.next!
        var prev = head
        prev.isTail = true
        while curr.isHead != true {
            let tmpNext = prev.next
            prev.next = prev.prev
            prev.prev = tmpNext
            prev = curr
            curr = curr.next!
        }
        let tmpNext = prev.next
        prev.next = prev.prev
        prev.prev = tmpNext
        prev.isHead = true
        head = prev
        curr.isHead = false
    }
    
}


var linked = LinkedList()
linked.insert(value: "5", index: 5)
linked.insert(value: "12", index: 2)
linked.insert(value: "15", index: 1)
linked.insert(value: "34", index: 76)
linked.insert(value: "54", index: 21)
linked.insert(value: "32", index: 7)
linked.insert(value: "22", index: 13)
linked.insert(value: "36", index: 73)
linked.nodeAt(index: 0)?.value
linked.reverse()
linked.nodeAt(index: 0)?.value


